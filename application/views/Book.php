<!-- 
-- Title - Books CRUD Application (For Aptify Use)
-- Created By - Ajay N. Tidake
-- Created date - 04/09/2018
-- Last Modified By - 
-- Last Modified Date -  
-->

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Books CRUD application</title>
        
        <!-- Extranal CSS - Start -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
        <!-- Extranal CSS - End -->
    </head>
    <body>
        <div class="container">
            <div class="row">
                <h3>Books CRUD application</h3>
            </div><br />
            
            <?php if($this->session->flashdata('success')): ?>
            <div class="row">
                <div class="alert alert-success alert-dismissible" style="width: 100%;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?>
                </div>
            </div>
            <?php endif; ?>
                
            <?php if($this->session->flashdata('error')): ?>
            <div class="row">
                <div class="alert alert-danger alert-dismissible" style="width: 100%;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <strong>Error! </strong><?php echo $this->session->flashdata('error'); ?>
                </div>
            </div>
            <?php endif; ?>
                
            <div class="row">
                <?php 
                $attributes = array('method' => 'post', 'onSubmit' => 'return true;');
                echo form_open(base_url().'index.php/BookController/check_action/', $attributes);
                ?>
                <table class="table table-bordered table-striped table-sm table-responsive-md">
                    <thead>
                        <tr>
                            <th width="05%" class="text-center">Sr.No</th>
                            <th width="25%" class="text-center">Book Title</th>
                            <th width="10%" class="text-center">Code</th>
                            <th width="10%" class="text-center">Author</th>
                            <th width="15%" class="text-center">Publisher</th>
                            <th width="10%" class="text-center">Publish Year</th>
                            <th width="05%" class="text-center">Price</th>
                            <th width="15%" class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $srno = 0;
                        foreach($book_list as $val){
                        ?>
                        <tr>
                            <td class="text-center"><?php echo ++$srno; ?></td>
                            <td><?php echo $val->book_title; ?></td>
                            <td><?php echo $val->book_code; ?></td>
                            <td><?php echo $val->book_author; ?></td>
                            <td><?php echo $val->book_publisher; ?></td>
                            <td><?php echo $val->book_publish_year; ?></td>
                            <td><?php echo $val->book_price; ?></td>
                            <td class="text-center">
                                <div style="padding: 2px;">
                                    <button type="button" class="btn btn-primary btn-sm" style="width: 150px;" onclick="fun_issue_book(<?php echo $val->id ?>);" <?php if($val->book_status != 'Available'): ?> disabled="disabled" <?php endif; ?> >Issue</button>
                                </div>
                                <div style="padding: 2px;">
                                    <button type="button" class="btn btn-primary btn-sm" style="width: 150px;" onclick="fun_return_book(<?php echo $val->id ?>);" <?php if($val->book_status != 'Issued'): ?> disabled="disabled" <?php endif; ?> >Return</button>
                                </div>
                                <div style="padding: 2px;">
                                    <button type="submit" class="btn btn-primary btn-sm" style="width: 150px;" name="btn_check_availability" value="<?php echo $val->id; ?>">Check Availability</button>
                                </div>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
                <?php echo form_close(); ?>
            </div>
        </div>
        
        <div class="modal fade" id="modal_issue_book" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <?php 
                    $attributes = array('method' => 'post', 'onSubmit' => 'return validate_form_issued();');
                    echo form_open(base_url().'index.php/BookController/check_action/', $attributes);
                    ?>
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Issue Book</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="txt_issued_to">Issued To:</label>
                                    <small><span class="text-danger" id="txt_issued_to_msg"></span></small>
                                    <input type="text" class="form-control input-sm" id="txt_issued_to" name="txt_issued_to" autocomplete="off" onkeypress="return onlyAlphabetsAndNumeric(event, this);" maxlength="250" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="txt_issued_by">Issued By:</label>
                                    <small><span class="text-danger" id="txt_issued_by_msg"></span></small>
                                    <input type="text" class="form-control input-sm" id="txt_issued_by" name="txt_issued_by" autocomplete="off" onkeypress="return onlyAlphabetsAndNumeric(event, this);" maxlength="250" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" style="width: 100px;" class="btn btn-secondary" data-dismiss="modal" value="Close">Close</button>
                        <button type="submit" style="width: 100px;" class="btn btn-primary" name="btn_issue_book" value="Submit">Submit</button>
                    </div>
                    <input type="hidden" name="hidd_tbl_book_id" id="hidd_tbl_book_id_issued" />
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="modal_return_book" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <?php 
                    $attributes = array('method' => 'post', 'onSubmit' => 'return validate_form_return();');
                    echo form_open(base_url().'index.php/BookController/check_action/', $attributes);
                    ?>
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Return Book</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="txt_return_comment">Return Comment:</label>
                                    <small><span class="text-danger" id="txt_return_comment_msg"></span></small>
                                    <input type="text" class="form-control input-sm" id="txt_return_comment" name="txt_return_comment" autocomplete="off" onkeypress="return onlyAlphabetsAndNumeric(event, this);" maxlength="250" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="txt_return_by">Return By:</label>
                                    <small><span class="text-danger" id="txt_return_by_msg"></span></small>
                                    <input type="text" class="form-control input-sm" id="txt_return_by" name="txt_return_by" autocomplete="off" onkeypress="return onlyAlphabetsAndNumeric(event, this);" maxlength="250" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" style="width: 100px;" class="btn btn-secondary" data-dismiss="modal" value="Close">Close</button>
                        <button type="submit" style="width: 100px;" class="btn btn-primary" name="btn_return_book" value="Submit">Submit</button>
                    </div>
                    <input type="hidden" name="hidd_tbl_book_id" id="hidd_tbl_book_id_return" />
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
        
        <!-- Extranal JS - Start -->
        <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>application/views/js/Book.js" type="text/javascript"></script>
        <!-- Extranal JS - End -->
    </body>
</html>

/**
 * Validation-  Allowed only alphabets
 */
function onlyAlphabets(e, t){
    try{
        if(window.event){
            var charCode = window.event.keyCode;
        }else if(e){
            var charCode = e.which;
        }else{
            return true;
        }
        if((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode == 0) || (charCode == 8) || (charCode == 32))
            return true;
        else
            return false;
    }catch(err){
    }
}

/**
 * Validation-  Allowed only alphabets and numeric characters
 */
function onlyAlphabetsAndNumeric(e, t){
    try{
        if(window.event){
            var charCode = window.event.keyCode;
        }else if(e){
            var charCode = e.which;
        }else{ 
            return true; 
        }
        if((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode == 0) || (charCode == 8 ) || (charCode == 32) || (charCode > 47 && charCode < 58))
            return true;
        else
            return false;
    }catch (err){
    }
}

/**
 * Validation-  Allowed only numeric digits
 */
function onlyNumeric(e, t){
    try{
        if(window.event){
            var charCode = window.event.keyCode;
        }else if(e){
            var charCode = e.which;
        }else{
            return true;
        }
        if((charCode == 0) || (charCode == 8) || (charCode > 47 && charCode < 58))
            return true;
        else
            return false;
    }catch(err){
    }
}

/**
 * Open issue book popup
 */
function fun_issue_book(tbl_book_id){
    if(tbl_book_id == ""){
        return false;
    }
    $("#txt_issued_to").val("");
    $("#txt_issued_by").val("");
    $("#txt_issued_to_msg").html("&nbsp;");
    $("#txt_issued_by_msg").html("&nbsp;");
    $("#hidd_tbl_book_id_issued").val(tbl_book_id);
    
    $("#modal_issue_book").modal("show");
}

/**
 * Validate book issue form before submit
 */
function validate_form_issued(){
    var txt_issued_to = $("#txt_issued_to").val();
    var txt_issued_by = $("#txt_issued_by").val();
    if(txt_issued_to.trim() == ""){
        $("#txt_issued_to_msg").text("Please enter value for issued to.");
        return false;
    }else{
        $("#txt_issued_to_msg").html("&nbsp;");
    }
    if(txt_issued_by.trim() == ""){
        $("#txt_issued_by_msg").text("Please enter value for issued by.");
        return false;
    }else{
        $("#txt_issued_by_msg").html("&nbsp;");
    }
    return true;
}

/**
 * Open return book popup
 */
function fun_return_book(tbl_book_id){
    if(tbl_book_id == ""){
        return false;
    }
    $("#txt_return_comment").val("");
    $("#txt_return_by").val("");
    $("#txt_return_comment_msg").html("&nbsp;");
    $("#txt_return_by_msg").html("&nbsp;");
    $("#hidd_tbl_book_id_return").val(tbl_book_id);
    
    $("#modal_return_book").modal("show");
}

/**
 * Validate book return form before submit
 */
function validate_form_return(){
    var txt_return_comment = $("#txt_return_comment").val();
    var txt_return_by = $("#txt_return_by").val();
    if(txt_return_comment.trim() == ""){
        $("#txt_return_comment_msg").text("Please enter value for return comment.");
        return false;
    }else{
        $("#txt_return_comment_msg").html("&nbsp;");
    }
    if(txt_return_by.trim() == ""){
        $("#txt_return_by_msg").text("Please enter value for return by.");
        return false;
    }else{
        $("#txt_return_by_msg").html("&nbsp;");
    }
    return true;
}

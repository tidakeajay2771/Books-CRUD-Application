<?php
/**
 * Title - Books CRUD Application (For Aptify Use)
 * Created By - Ajay N. Tidake
 * Created date - 04/09/2018
 * Last Modified By - 
 * Last Modified Date -  
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class BookModel extends CI_Model {
    
    /**
     * GET ALL RECORDS
     * 
     * Fetch record from 'tbl_book' and 'tbl_master_book_status' 
     * @return      array of objects
     */
    public function get_all_records()
    {
        $query = $this->db->select('t1.id, t1.book_title, t1.book_code, t1.book_author, t1.book_publisher, t1.book_publish_year, t1.book_price, t2.book_status')
                                ->from('tbl_book as t1')
                                ->join('tbl_master_book_status as t2', 't1.tbl_master_book_status_id = t2.id')
                                ->where('t1.isActive', 1)
                                ->where('t2.isActive', 1)
                                ->get();
        return $query->result();
    }
    
    /**
     * ISSUE BOOK
     * 
     * Insert record in 'tbl_book_operations_log' and update in 'tbl_book'
     * @param       array 
     * @return      boolean 
     */
    public function issue_book($userData)
    {
        $query = $this->db->select('id')
                                ->from('tbl_master_book_status')
                                ->where('book_status', 'Issued')
                                ->where('isActive', 1)
                                ->get();
        $result = $query->row();
        $tbl_master_book_status_id = $result->id;
        
        $insert_data = array(
            'tbl_book_id' => $userData['hidd_tbl_book_id'], 
            'tbl_master_book_status_id' => $tbl_master_book_status_id, 
            'issued_to' => $userData['txt_issued_to'], 
            'issued_by' => $userData['txt_issued_by'], 
            'issued_date' => date('Y-m-d'), 
            'created_by' => 'SYSTEM', 
            'created_date_time' => date('Y-m-d H:i:s'), 
            'modified_by' => 'SYSTEM', 
            'modified_date_time' => date('Y-m-d H:i:s'), 
            'isActive' => 1 
        );
        $query = $this->db->insert("tbl_book_operations_log", $insert_data);
        if($query){
            $this->db->set('tbl_master_book_status_id', $tbl_master_book_status_id);
            $this->db->set('modified_by', 'SYSTEM');
            $this->db->set('modified_date_time', date('Y-m-d H:i:s'));
            $this->db->where('id', $userData['hidd_tbl_book_id']);
            $query = $this->db->update('tbl_book');
            if($query){
                return true;
            }else{
                $this->db->set('modified_by', 'SYSTEM');
                $this->db->set('modified_date_time', date('Y-m-d H:i:s'));
                $this->db->set('isActive', 0);
                $this->db->where('id', $this->db->insert_id());
                $this->db->update('tbl_book_operations_log');
                
                return false;
            }
        }else{
            return false;
        }
    }
    
    /**
     * RETURN BOOK
     * 
     * Insert record in 'tbl_book_operations_log' and update in 'tbl_book'
     * @param       array 
     * @return      boolean 
     */
    public function return_book($userData)
    {
        $query = $this->db->select('id')
                                ->from('tbl_master_book_status')
                                ->where('book_status', 'Available')
                                ->where('isActive', 1)
                                ->get();
        $result = $query->row();
        $tbl_master_book_status_id = $result->id;
        
        $insert_data = array(
            'tbl_book_id' => $userData['hidd_tbl_book_id'], 
            'tbl_master_book_status_id' => $tbl_master_book_status_id,
            'return_comment' => $userData['txt_return_comment'], 
            'return_by' => $userData['txt_return_by'], 
            'return_date' => date('Y-m-d'), 
            'created_by' => 'SYSTEM', 
            'created_date_time' => date('Y-m-d H:i:s'), 
            'modified_by' => 'SYSTEM', 
            'modified_date_time' => date('Y-m-d H:i:s'), 
            'isActive' => 1 
        );
        $query = $this->db->insert("tbl_book_operations_log", $insert_data);
        if($query){
            $this->db->set('tbl_master_book_status_id', $tbl_master_book_status_id);
            $this->db->set('modified_by', 'SYSTEM');
            $this->db->set('modified_date_time', date('Y-m-d H:i:s'));
            $this->db->where('id', $userData['hidd_tbl_book_id']);
            $query = $this->db->update('tbl_book');
            if($query){
                return true;
            }else{
                $this->db->set('modified_by', 'SYSTEM');
                $this->db->set('modified_date_time', date('Y-m-d H:i:s'));
                $this->db->set('isActive', 0);
                $this->db->where('id', $this->db->insert_id());
                $this->db->update('tbl_book_operations_log');
                
                return false;
            }
        }else{
            return false;
        }
    }
    
    /**
     * CHECK BOOK AVAILABILITY
     * 
     * Get total count from 'tbl_book' table and validate on that basis
     * @param       array 
     * @return      boolean 
     */
    function check_book_availability($userData)
    {
        $query = $this->db->select('id')
                                ->from('tbl_master_book_status')
                                ->where('book_status', 'Available')
                                ->where('isActive', 1)
                                ->get();
        $result = $query->row();
        $tbl_master_book_status_id = $result->id;
        
        $query = $this->db->select('count(*) as total_row_count')
                                ->from('tbl_book')
                                ->where('id', $userData['hidd_tbl_book_id'])
                                ->where('tbl_master_book_status_id', $tbl_master_book_status_id)
                                ->get();
        $result = $query->row();
        if($result->total_row_count == 1){
            return true;
        }else{
            return false;
        }
    }
}

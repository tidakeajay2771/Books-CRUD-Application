<?php
/**
 * Title - Books CRUD Application (For Aptify Use)
 * Created By - Ajay N. Tidake
 * Created date - 04/09/2018
 * Last Modified By - 
 * Last Modified Date -  
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class BookController extends CI_Controller{
    
    /**
     * INDEX
     * 
     * Default method - Get records from model and load the view
     */
    public function index()
    {
        $this->load->model('BookModel');
        $data['book_list'] = $this->BookModel->get_all_records();
        $this->load->view('Book', $data);
    }
    
    /**
     * CHECK ACTION
     * 
     * Validate form post action and call related function. In case if error occurred redirect to default controller (Mentioned in routes.php)
     */
    public function check_action(){
        if($this->input->post('btn_issue_book')){
            $this->issue_book();
        }else if($this->input->post('btn_return_book')){
            $this->return_book();
        }else if($this->input->post('btn_check_availability')){
            $this->check_book_availability();
        }else{
            $this->session->set_flashdata('error', 'Invalid request. Please try again!!!');
            redirect();
        }
    }
    
    /**
     * ISSUE BOOK
     * 
     * Call issue book model method and send response to view. In case if error occurred redirect to default controller (Mentioned in routes.php)
     */
    public function issue_book()
    {
        if($this->input->post('btn_issue_book')){
            $userData = array(
                'txt_issued_to' => strip_tags($this->input->post('txt_issued_to')), 
                'txt_issued_by' => strip_tags($this->input->post('txt_issued_by')), 
                'hidd_tbl_book_id' => strip_tags($this->input->post('hidd_tbl_book_id')) 
            );
            $this->load->model('BookModel');
            $response = $this->BookModel->issue_book($userData);
            if($response){
                $this->session->set_flashdata('success', 'Book issued successfully. Thank You!!!');
            }else{
                $this->session->set_flashdata('error', 'Book not issued. Please try again!!!');
            }
        }
        redirect();
    }
    
    /**
     * RETURN BOOK
     * 
     * Call return book model method and send response to view. In case if error occurred redirect to default controller (Mentioned in routes.php)
     */
    public function return_book()
    {
        if($this->input->post('btn_return_book')){
            $userData = array(
                'txt_return_comment' => strip_tags($this->input->post('txt_return_comment')), 
                'txt_return_by' => strip_tags($this->input->post('txt_return_by')), 
                'hidd_tbl_book_id' => strip_tags($this->input->post('hidd_tbl_book_id')) 
            );
            $this->load->model('BookModel');
            $response = $this->BookModel->return_book($userData);
            if($response){
                $this->session->set_flashdata('success', 'Book returned successfully. Thank You!!!');
            }else{
                $this->session->set_flashdata('error', 'Book not returned. Please try again!!!');
            }
        }
        redirect();
    }
    
    /**
     * CHECK BOOK AVAILABILITY
     * 
     * Call check book availability model method and send response to view. In case if error occurred redirect to default controller (Mentioned in routes.php)
     */
    public function check_book_availability(){
        if($this->input->post('btn_check_availability')){
            $userData = array(
                'hidd_tbl_book_id' => strip_tags($this->input->post('btn_check_availability')) 
            );
            $this->load->model('BookModel');
            $response = $this->BookModel->check_book_availability($userData);
            if($response){
                $this->session->set_flashdata('success', 'Book available in library.');
            }else{
                $this->session->set_flashdata('error', 'Book not available in library.');
            }
        }
        redirect();
    }
}

CREATE DATABASE  IF NOT EXISTS `db_crud_app` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_crud_app`;
-- MySQL dump 10.13  Distrib 5.5.61, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: db_crud_app
-- ------------------------------------------------------
-- Server version	5.5.61-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_book`
--

DROP TABLE IF EXISTS `tbl_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_master_book_status_id` int(11) DEFAULT NULL,
  `book_title` varchar(255) DEFAULT NULL,
  `book_code` varchar(255) DEFAULT NULL,
  `book_author` varchar(255) DEFAULT NULL,
  `book_publisher` varchar(255) DEFAULT NULL,
  `book_publish_year` year(4) DEFAULT NULL,
  `book_price` decimal(10,0) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date_time` datetime DEFAULT NULL,
  `isActive` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_book_tbl_master_book_status_id_idx` (`tbl_master_book_status_id`),
  CONSTRAINT `tbl_book_tbl_master_book_status_id` FOREIGN KEY (`tbl_master_book_status_id`) REFERENCES `tbl_master_book_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_book`
--

LOCK TABLES `tbl_book` WRITE;
/*!40000 ALTER TABLE `tbl_book` DISABLE KEYS */;
INSERT INTO `tbl_book` VALUES (1,1,'Networking','NET','James','Rohan Rathore',2018,100,'SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-05 01:24:16',''),(2,1,'Malory Towers','MT','ANIRBAN BOSE','Harper Collins',2018,150,'SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-05 01:23:57',''),(3,1,'Robin Sharma Who Sold His Ferrari','RSWSHF','Jyotsna Bharti','Nirali publisher',2018,120,'SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-04 00:00:00',''),(4,1,'Science For X Biology','SFXB','Shyam Dua ','Hind Pocket books',2018,100,'SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-04 00:00:00',''),(5,1,'The Rainbow','TR','MICHARL T. GILBERT','Jaico Publication.',2018,250,'SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-04 00:00:00',''),(6,1,'Treasure Island','TI','PAT & CARD MC GREAL','Rathuga Prakashan',2018,400,'SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-04 00:00:00',''),(7,1,'Why Be Unfriendly','WBU','DAVE RAWSON','Little Schollarz Pvt. Ltd.',2018,100,'SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-04 00:00:00',''),(8,1,'Mere Desh Ki Mahaan Naria','MDKMN','MICHARL T. GILBERT','MOLHOTRA BOOK DEPOT',2018,70,'SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-04 00:00:00',''),(9,1,'MERA SHISHU GEET','MSG','Disney','SARASWATI HOUSE',2018,100,'SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-04 00:00:00',''),(10,1,'Mermaild','M','Om kidz','Rakesh Publications',2018,120,'SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-04 00:00:00',''),(11,1,'MERRY CHRISTMAS GERONIMD','MCG','Dr. Madhuri','AMAR CHITRAKATHA PUT',2018,150,'SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-04 00:00:00',''),(12,1,'Merry Querry','MQ','NEENA SINHANA','HARPER COLLINS PUB',2018,130,'SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-04 00:00:00',''),(13,1,'Methodofogy of Educational Resarch','MOER','R.S Mittal','C.B.T.',2018,100,'SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-04 00:00:00',''),(14,1,'MICE IN MAN ','MIN','SUBRUTO BAGCHI','KIRTE PUBLICATION',2018,100,'SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-04 00:00:00',''),(15,1,'Michael Faraday','MF','SATGURU GUPTA','SCHOLASTIC CHILDREN\'S BOOKS',2018,230,'SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-05 01:24:27','');
/*!40000 ALTER TABLE `tbl_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_book_operations_log`
--

DROP TABLE IF EXISTS `tbl_book_operations_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_book_operations_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_book_id` int(11) DEFAULT NULL,
  `tbl_master_book_status_id` int(11) DEFAULT NULL,
  `issued_to` varchar(255) DEFAULT NULL,
  `issued_by` varchar(255) DEFAULT NULL,
  `issued_date` date DEFAULT NULL,
  `return_comment` varchar(255) DEFAULT NULL,
  `return_by` varchar(255) DEFAULT NULL,
  `return_date` date DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date_time` datetime DEFAULT NULL,
  `isActive` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tbl_book_operations_log_tbl_book_id_idx` (`tbl_book_id`),
  KEY `tbl_book_operations_log_tbl_master_book_status_id_idx` (`tbl_master_book_status_id`),
  CONSTRAINT `tbl_book_operations_log_tbl_book_id` FOREIGN KEY (`tbl_book_id`) REFERENCES `tbl_book` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_book_operations_log_tbl_master_book_status_id` FOREIGN KEY (`tbl_master_book_status_id`) REFERENCES `tbl_master_book_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_book_operations_log`
--

LOCK TABLES `tbl_book_operations_log` WRITE;
/*!40000 ALTER TABLE `tbl_book_operations_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_book_operations_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_master_book_status`
--

DROP TABLE IF EXISTS `tbl_master_book_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_master_book_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_status` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `modified_date_time` datetime DEFAULT NULL,
  `isActive` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_master_book_status`
--

LOCK TABLES `tbl_master_book_status` WRITE;
/*!40000 ALTER TABLE `tbl_master_book_status` DISABLE KEYS */;
INSERT INTO `tbl_master_book_status` VALUES (1,'Available','SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-04 00:00:00',''),(2,'Issued','SYSTEM','2018-09-04 00:00:00','SYSTEM','2018-09-04 00:00:00','');
/*!40000 ALTER TABLE `tbl_master_book_status` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-05  1:35:12
